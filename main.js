// $(function(){
//  var navbar2 = $("#navbar2");
//     console.log(navbar1);


//  $(window).scroll(function(){
//    if($(window).scrollTop() >= 100){
//      navbar2.addClass('displayblock');
//    } else{
//      navbar2.removeClass('displayblock');
//    }


// });


// })
let mainNavLinks = document.querySelectorAll("nav_item");
let mainSections = document.querySelectorAll("main section");

let lastId;
let cur = [];

// This should probably be throttled.
// Especially because it triggers during smooth scrolling.
// https://lodash.com/docs/4.17.10#throttle
// You could do like...
// window.addEventListener("scroll", () => {
//    _.throttle(doThatStuff, 100);
// });
// Only not doing it here to keep this Pen dependency-free.

window.addEventListener("scroll", event => {
  let fromTop = window.scrollY;

  mainNavLinks.forEach(link => {
    let section = document.querySelector(link.hash);

    if (
      section.offsetTop <= fromTop &&
      section.offsetTop + section.offsetHeight > fromTop
    ) {
      link.classList.add("current");
    } else {
      link.classList.remove("current");
    }
  });
});
